<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function __construct()
    {

    }

    public function index(Request $request){
        if(Auth::check()){
            return to_route('dashboard');
        }
        return view('pages.index');
    }

    public function register(Request $request){
        return view('pages.register');
    }

    public function createAccount(Request $request){
        $rules = array(
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'role' => 'required',
            'password' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return back()->with('error', $validator->errors()->first());
        }
        $user = new User();
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->password=Hash::make($request->get('password')) ;
        $user->role = $request->get('role');
        $user->save();
        return to_route('index')->with('success', 'Successfully Login');
    }

    public function login(Request $request){
        $rules = array(
            'email' => 'required',
            'password' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return back()->with('error', $validator->errors()->first());
        }
        if (Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ])){
            Session::put('user_role',Auth::user()->role);
            return to_route('dashboard');
        }
        else{
            return to_route('index')->with('error', 'Successfully invalid user');
        }

    }

    public function dashboard(Request $request){
        return view('pages.dashboard');
    }

    public function logout(Request $request){
        Session::forget('user_role');
        Auth::logout();
        return to_route('index');
    }

    public function changeRole(Request $request){
        return view('pages.change_role');
    }

    public function updateRole(Request $request){
        Session::put('user_role',$request->get('role'));
        return to_route('dashboard');
    }

    public function getRolesDetails(Request $request,$userRoles){
        $user = User::where('role',$userRoles)->get();
        return view('pages.user_role_details',['user' => $user]);
    }
}
