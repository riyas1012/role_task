<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::controller(UserController::class)->group(function () {
    Route::get('/', 'index')->name('index');
    Route::get('/register', 'register')->name('register');
    Route::post('/create-account', 'createAccount')->name('create.account');
    Route::post('/login', 'login')->name('login');
    Route::middleware(['user_auth'])->group(function () {
        Route::get('/dashboard','dashboard')->name('dashboard');
        Route::get('change-role','changeRole')->name('change_role')->middleware('role_check');
        Route::post('update-role','updateRole')->name('update_role');
        Route::get('user-logout','logout')->name('logout');
        Route::get('{roles}','getRolesDetails')->name('roles_details');
    });
});


