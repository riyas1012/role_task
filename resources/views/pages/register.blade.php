@extends('layout.app_base')
@section('content')

<div class="login-box">
    <div class="login-logo login-logo1">
        <h1>
            Role Task
        </h1>
    </div>
    <div class="login-box-body">
        <h3 class="login-box-msg">
            Register
        </h3>
        @include('pages.flash-message')
        <form name="register_form" method="post" action="{{route('create.account')}}">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <label>
                    Name
                    <span class="mandatorycolor">
                        *
                    </span>
                </label>
                <input type="text" name="name" id="name" class="form-control login-box-input"
                 placeholder="Name" value="{{old('name')}}">
            </div>
            <div class="form-group has-feedback">
                <label>
                    Email
                    <span class="mandatorycolor">
                        *
                    </span>
                </label>
                <input type="email" name="email" id="email" class="form-control login-box-input"
                 placeholder="Email" value="{{old('name')}}">
            </div>
            <div class="form-group has-feedback">
                <label>
                    Password
                    <span class="mandatorycolor">
                        *
                    </span>
                </label>
                <input type="password" name="password" id="password" class="form-control login-box-input"
                placeholder="Password" value="{{old('password')}}">
            </div>
            <div class="form-group has-feedback">
                <label>
                    Role
                    <span class="mandatorycolor">
                        *
                    </span>
                </label>
                <select name="role" id="role" class="form-control login-box-input">
                    <option value="">Select Role</option>
                    <option value="Administrator">Administrator</option>
                    <option value="Student">Student</option>
                    <option value="Staff">Staff</option>
                </select>
            </div>
            <div class="row">
                <div class="col-xs-12 account-info">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        <a style="color: white;">
                            Register
                        </a>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 account-info" style="color: white;">
        <a href="{{route('index')}}">
            <p class="p-text">
                Already have an account? &nbsp;&nbsp; <b>Login </b>
            </p>
        </a>
    </div>
</div>
<script>
    $(function(){
        $("form[name='register_form']").validate({
            rules: {
                name : {
                    required: true
                },
                password: {
                    required: true
                },
                email: {
                    required: true
                },
                role: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Please enter Name"
                },
                password: {
                    required: "Please enter Password",
                    minlength: 'Password must be at least 8 characters long.',
                    maxlength: 'Password should not exceed 32 characters.'
                },
                email: {
                    required: "Please enter Email"
                },
                role: {
                    required: "Please select Role"
                },
            },
            submitHandler : function(form){
                form.submit();
            },
            invalidHandler: function(form,validator){
                errorhandler(form,validator);
            }
        })
    });
</script>
@stop
