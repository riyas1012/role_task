<script>
    $(document).ready(function(){
        toastr.options.closeButton = true;
        toastr.options.progressBar = true;
        toastr.options.timeOut = 200000;
        toastr.options.extendedTimeOut = 5000;
    })
</script>

@if ($message = Session::get('success'))
    <script>
        $(document).ready(function(){
            console.log("success");
            toastr.success('{{ $message }}')
        })
    </script>
@endif

@if ($message = Session::get('error'))
    <script>
        $(document).ready(function(){
            toastr.error('{{ $message }}')
        })
    </script>
@endif
