@extends('layout.innerpage')
@section('content')

<div class="login-box">
    <div class="login-logo login-logo1">
        <h1>
            Role Task
        </h1>
    </div>
    <div class="login-box-body">
        <h3 class="login-box-msg">
            Change Role
        </h3>
        @include('pages.flash-message')
        <form name="register_form" method="post" action="{{route('update_role')}}">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <label>
                    Role
                </label>
                <select name="role" id="role" class="form-control login-box-input">
                    <option value="Administrator" @if(Session::get('user_role')=='Administrator') selected @endif>Administrator</option>
                    <option value="Student" @if(Session::get('user_role')=='Student') selected @endif>Student</option>
                    <option value="Staff" @if(Session::get('user_role')=='Staff') selected @endif>Staff</option>
                </select>
            </div>
            <div class="row">
                <div class="col-xs-12 account-info">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        <a style="color: white;">
                            Change Role
                        </a>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
