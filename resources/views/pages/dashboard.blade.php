@extends('layout.innerpage')
@section('content')

<div class="login-box">
    <div class="login-logo login-logo1">
        <h1>
            Role Task
        </h1>

    </div>
    {{-- <div class="login-box-body">
        Welcome {{Auth::user()->name}} as {{Auth::user()->role}}
        @include('pages.flash-message')
    </div> --}}
</div>
<div class="col-md-12 account-info">
    @include('pages.flash-message')
    <h2>
        Welcome {{Auth::user()->name}} as {{Auth::user()->role}}
    </h2>
</div>
@stop
