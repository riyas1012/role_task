@extends('layout.innerpage')
@section('content')

<section class="content">
    @include('pages.flash-message')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        {{Session::get('user_role')}} List
                    </h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="userList" class="table table-bordered table-striped">
                            <thead>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Role
                                </th>
                                <th>
                                    Registered On
                                </th>
                            </thead>
                            <tbody>
                                @foreach ($user as $userItem)
                                    <tr>
                                        <td>
                                            {{$userItem->name}}
                                        </td>
                                        <td>
                                            {{$userItem->email}}
                                        </td>
                                        <td>
                                            {{$userItem->role}}
                                        </td>
                                        <td>
                                            {{ $userItem->created_at }}

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(function(){
        $("#userList").DataTable({
            responsive: !0,
            "aaSorting":[[3,"desc"]],
        })
    })
</script>
@stop
