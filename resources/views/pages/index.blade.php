@extends('layout.app_base')
@section('content')

<div class="login-box">
    <div class="login-logo login-logo1">
        <h1>
            Role Task
        </h1>
    </div>
    <div class="login-box-body">
        <h3 class="login-box-msg">
            Login
        </h3>
        @include('pages.flash-message')
        <form name="signin" method="post" action="{{route('login')}}">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <label>
                    Email
                    <span class="mandatorycolor">
                        *
                    </span>
                </label>
                <input type="text" name="email" id="email" class="form-control login-box-input"
                 placeholder="Email" value="{{old('email')}}">
            </div>
            <div class="form-group has-feedback">
                <label>
                    Password
                    <span class="mandatorycolor">
                        *
                    </span>
                </label>
                <input type="password" name="password" id="password" class="form-control login-box-input"
                placeholder="Password" value="{{old('password')}}">
            </div>
            <div class="row">
                <div class="col-xs-12 account-info">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        <a style="color: white;">
                            Sign In
                        </a>
                    </button>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-xs-12 text-left">
                    <a href="{{route('register')}}">
                        Create Account
                    </a>
                </div>
            </div> --}}
        </form>
    </div>
    <div class="col-xs-12 account-info" style="color: white;">
        <a href="{{route('register')}}">
            <p class="p-text">
                Don't have an account? &nbsp;&nbsp; <b>Register </b>
            </p>
        </a>
    </div>
</div>
<script>
    $(function(){
        $("form[name='signin']").validate({
            rules: {
                email : {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                email: {
                    required: "Please enter Email"
                },
                password: {
                    required: "Please enter Password",
                    minlength: 'Password must be at least 8 characters long.',
                    maxlength: 'Password should not exceed 32 characters.'
                }
            },
            submitHandler : function(form){
                form.submit();
            },
            invalidHandler: function(form,validator){
                errorhandler(form,validator);
            }
        })
    });
</script>
@stop
