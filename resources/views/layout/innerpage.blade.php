<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
  <title>Role Task</title>
  {{-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/dist/img/mono.svg') }}"> --}}
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('assets/dist/css/AdminLTE.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/dist/css/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    {{-- <link rel="stylesheet" href="{{asset('assets/css/crop-image.css')}}"> --}}
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/toastr.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
{{-- <script src="{{ asset('assets/dist/js/sweetalert2.all.min.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css"> --}}

  <script>
      function validateonlychar(txt) {
            txt.value = txt.value.replace(/[^a-zA-Z \n\r]+/g, '');
        }
        function validateonlynumber(txt) {
          var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
        return true;
    }
        }
        function validateonlycharnumber(txt) {
          txt.value = txt.value.replace(/[^a-zA-Z0-9\n\r]+/g, '');
          if(txt.value == '') {
            $('#available-icon').hide();
            $('#available-msg').hide();
            $('#exist-icon').hide();
            $('#exist-msg').hide();
          }
        }
        function validateusername(txt) {
          txt.value = txt.value.replace(/[^a-zA-Z0-9_\-@.\n\r]+/g, '');
          if(txt.value == '') {
            $('#available-icon').hide();
            $('#available-msg').hide();
            $('#exist-icon').hide();
            $('#exist-msg').hide();
          }
        }
        function spacialcharvalidation(txt) {
          txt.value = txt.value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        }
        function subdomainValidate(txt) {
          txt.value = txt.value.replace(/[^a-zA-Z0-9\-@.*\n\r]+/g, '');
        }
  </script>
<style>
#loading {
   width: 100%;
   height: 100%;
   top: 0;
   left: 0;
   position: fixed;
   display: block;
   opacity: 0.7;
   background-color: #fff;
   z-index: 99;
   text-align: center;
}

#loading-image {
  position: absolute;
  top: 220px;

  z-index: 100;
}
body{-ms-overflow-style: scrollbar !important;}

</style>
@if (!empty($tagManager) && $signupComplete==1)
  @foreach($tagManager as $tag)
    @if ($tag['position'] == config("config.PAGE_POSITION_HEAD"))
      {!! html_entity_decode($tag['script']) !!}
    @endif
  @endforeach
@endif
</head>
   <body class="hold-transition skin-blue sidebar-mini fixed">
   @if (!empty($tagManager) && $signupComplete==1)
    @foreach($tagManager as $tag)
      @if ($tag['position'] == config("config.PAGE_POSITION_BODY_TOP"))
        {!! html_entity_decode($tag['script']) !!}
      @endif
    @endforeach
  @endif
   <div id="loading" style="display:none;">
  <img id="loading-image" src="{{ asset('assets/img/loading.svg') }}" alt="Loading..." />
</div>

<div class="wrapper">
    @include('layout.includes.header')
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    @include('layout.includes.leftmenu')
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  @if(Session::has('message'))
  <div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;"><a class="pull-right" href="#" data-toggle="tooltip" data-placement="left" title="Never show me this again!" style="color: rgb(255, 255, 255); font-size: 20px;">×</a><a href="https://themequarry.com" style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">{{ Session::get('message') }}</div>
  @endif
    <!-- Content Header (Page header) -->
      @yield('content')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   {{-- @include("layouts.includes.footer") --}}


  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<!-- <script src="{{ asset('assets/finalversion/plugins.js') }}"></script> -->

<script src="{{ asset('assets/bower_components/jquery/dist/jquery.min.js') }}"></script>

<script src="{{ asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>


<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>


<script src="{{ asset('assets/bower_components/jquery-knob/js/jquery.knob.js') }}"></script>

<script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>

<script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

<!-- page script -->
<script>
  $(function () {
    /* jQueryKnob */

 setTimeout(function(){ $(".alert-success").hide(); }, 25000);
 setTimeout(function(){ $(".alert-danger").hide(); }, 25000);
 setTimeout(function(){ $(".alert-warning").hide(); }, 25000);
 setTimeout(function(){ $(".alert-info").hide(); }, 25000);
    // $.getJSON("https://api.ipify.org?format=json", function(data) {
    //     var ipaddress = data.ip;
    // var autoDet = Intl.DateTimeFormat().resolvedOptions().timeZone;
    // $.get('/upateipaddress', {timezone:autoDet, ipaddress:ipaddress}).done(function( data ) {
    //     if (data != '') {
    //     var returnJson = JSON.parse(data);
    //     if (returnJson.findStatus == 'auto_detection') {
    //       $('.add_timezone').text(autoDet);
    //       var str = "{{Lang::get('labels.autodetection_msg')}}";
    //       var res = str.replace(":autodetectedtime", autoDet);
    //       //toastr.info(res, '', {"iconClass": 'toast-info info-msg'});
    //         toastr.info(res);
    //     }
    //     if (returnJson.findStatus == 'not_auto_detection') {
    //       var str = "{{Lang::get('labels.autodetection_msg')}}";
    //       var res = str.replace(":autodetectedtime", returnJson.timezone);
    //       toastr.info(res);
    //     }
    //   }
    // });
    // });

    $(".knob").knob({

    });
    /* END JQUERY KNOB */


//Date picker
$('#from_date').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
    })

$('#to_date').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
    })

  });

</script>
<script>
  $(function () {
/*{
        responsive: !0,
         columnDefs: [
               { orderable: false, targets: [-1] }
            ]
      }*/
    $('#example1').DataTable()
    var pageWidth = $(window).width();
    if (pageWidth > 1024) {
        $('.table-responsive').on('show.bs.dropdown', function () {
       $('.table-responsive').css( "overflow", "inherit" ); });
       $('.table-responsive').on('hide.bs.dropdown', function () {
       $('.table-responsive').css( "overflow", "auto" );
       })
    }

  })



  $(function () {
  $('[data-toggle="popover"]').popover({ trigger: "hover" })

})

$("#vm_circle").knob({
  'format' : function (value) {
   var values = $("#vm_circle").attr("label");
     return values;
  }
});
$("#sshkey_circle").knob({
  'format' : function (value) {
   var values = $("#sshkey_circle").attr("label");
     return values;
  }
});
$("#childuser_circle").knob({
  'format' : function (value) {
   var values = $("#childuser_circle").attr("label");
     return values;
  }
});
$("#firewall_circle").knob({
  'format' : function (value) {
   var values = $("#firewall_circle").attr("label");
     return values;
  }
});

$("#microvps_circle").knob({
  'format' : function (value) {
   var values = $("#microvps_circle").attr("label");
     return values;
  }
});

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
$(document).ready(function(){
$("[data-hover='tooltip']").tooltip();
});

function errorhandler(form, validator){
  var errors = validator.numberOfInvalids();
      if (errors) {
        var message = errors == 1;
        var errors = "<b>{{ __('jsvalidate.validation_failed') }}</b> <br>";
        if (validator.errorList.length > 0) {
            for (x=0;x<validator.errorList.length;x++) {
                errors += "\n\u25CF " + validator.errorList[x].message +"<br>";
            }
        }
        toastr.options.closeButton = true;
        toastr.options.progressBar = true;
        toastr.options.timeOut = 5000;
        toastr.options.extendedTimeOut = 1000;
        toastr.error(errors);
      }
      validator.focusInvalid();
}
  $.validator.addMethod("emailValidate", function(value, element) {
      var regex = /^([a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(value);
    }, "{{ __('jsvalidate.email_valid') }}");
</script>

 <style>
    form .error{
        color: #ff0000;
    }

    .form-control {
        padding: 6px 0px !important;
    }
    .form-control[readonly] {
        background-color: #EEE !important;
        opacity: 1;
    }
</style>
  @if (!empty($tagManager) && $signupComplete==1)
    @foreach($tagManager as $tag)
      @if ($tag['position'] == config("config.PAGE_POSITION_BODY_BOTTOM"))
        {!! html_entity_decode($tag['script']) !!}
      @endif
    @endforeach
  @endif
</body>


</html>

