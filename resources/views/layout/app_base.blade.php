
<!DOCTYPE html>
<html>
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <head>
        <meta charset="utf-8">
        <title>
            Role Task
        </title>
        <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/dist/css/toastr.min.css')}}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
        <link rel="stylesheet" href="{{asset('assets/dist/css/AdminLTE.min.css')}}">
        <meta name="csrf" value="{{csrf_token()}}">
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
        <script src="{{asset('assets/js/toastr.min.js')}}"></script>
    </head>
    <body class="hold-transition" style="height: fit-content">
        @yield('content')
        <script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
        <script src="{{asset('assets/js/additional-methods.min.js')}}"></script>
        <style>
            form .error{
                color: #ff0000;
            }

            .form-control {
                padding: 6px 0px !important;
            }
        </style>
        <script>
            function errorhandler(from, validator){
                var errors = validator.numberOfInvalids();
                if(errors){
                    var message = errors = 1;
                    var errors = "<b>Validation failed</b> <br>";
                    if(validator.errorList.length > 0) {
                        for (x=0;x<validator.errorList.length;x++)
                        {
                            errors += "\n\u25CF "+ validator.errorList[x].message + "<br>";
                        }
                    }
                    toastr.options.closeButton = true;
                    toastr.options.progressBar = true;
                    toastr.options.timeOut = 5000;
                    toastr.options.extendedTimeOut = 1000;
                    toastr.error(errors);
                }
                validator.focusInvalid();
            }
            $.validator.addMethod("emailValidate", function(value, element){
                var regex = /^([a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(value);
            },"Please enter valid Email address.")
        </script>
        @stack('page_js')
    </body>
</html>
