<section class="sidebar" id=main-sidebar>
    <div class="user-panel">
    </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li @if (Route::is('dashboard')) class="active" @endif>
                <a href="{{route('dashboard')}}">
                    <i class="fa fa-dashboard"></i><span>&nbsp;Dashboard</span>
                </a>
            </li>
            <li @if (Route::is('roles_details',['roles'=>Session::get('user_role')])) class="active" @endif>
                <a href="{{route('roles_details',['roles'=>Session::get('user_role')])}}">
                    <i class="fa fa-dashboard"></i><span>{{Session::get('user_role')}}</span>
                </a>
            </li>

            {{-- <li @if (Request::segment(2)=='category') class="active" @endif>
                <a href="{{url('/admin/category')}}">
                    <i class="fa fa-dashboard"></i><span>&nbsp;Category</span>
                </a>
            </li>
            <li @if (Request::segment(2)=='product') class="active" @endif>
                <a href="{{url('/admin/product')}}">
                    <i class="fa fa-shopping-bag" aria-hidden="true"></i><span>&nbsp;Product</span>
                </a>
            </li> --}}
        </ul>
</section>
