<header class="main-header">
    <a href="{{ route('index') }}" class="logo" style="color: black !important">

        Role Task

    </a>
    <nav class="navbar navbar-static-top effect1" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" data-hover="tooltip" data-placement="right"
            title="Cpllapse / Expand side menu" role="button" data-trigger="hover">
            <span class="sr-only">
                Toggle navigation
            </span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user-circle-o" aria-hidden="true">
                        </i>
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu" style="left: auto;">
                        <li role="presentation">
                            <a role="menuitem" tabindex="-1" href="{{ route('logout') }}">Logout
                            </a>
                        </li>
                        @if (Auth::user()->role == 'Administrator')
                            <li @if (Route::is('change_role')) class="active" @endif>
                                <a href="{{ route('change_role') }}">
                                    <i class="fa fa-dashboard"></i><span>Change Role</span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            </ul>

        </div>
    </nav>
</header>
